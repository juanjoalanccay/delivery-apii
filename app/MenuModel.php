<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuModel extends Model
{
    public static function validateFields($object)
    {
        return is_null($object->name) || is_null($object->details) || is_null($object->price);
    }
    protected $table ='menu';
    protected $fillable=array('name','details','price','photo');
    public $timestamps = false;//cancela los campos automaticos created_at y updated_at

    public function commentaries()
    {
        return $this->hasMany(CommentaryModel::class,'id_menu');
    }
    
}
