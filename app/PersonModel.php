<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonModel extends Model
{
    public static function validateFields($object)
    {
        return is_null($object->name) || is_null($object->last_name)
            || is_null($object->email) || is_null($object->phone) || is_null($object->password)
            || is_null($object->dni);
    }
   
    protected $table ='person';
    protected $fillable=array('name','last_name','email','phone','password','photo','dni');
    public $timestamps = false;//cancela los campos automaticos created_at y updated_at
    
    public function delivery()
    {
        return $this->hasOne(DeliveryModel::class,'id_person');
    }
    public function user()
    {
        return $this->hasOne(UserModel::class,'id_person');
    }
}
