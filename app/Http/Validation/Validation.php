<?php
namespace App\Http\Validation;

use Exception;

class Validation
{

    public static function determinateError(Exception $e)
    {
        $string = $e->getMessage();
        $pos = strpos($string, "violation:"); //ocupa 10 
        $msg = $string;
        if (strpos($string, "1062") != false) { //numero de error de unique
            $msg = 'Valor unico violado: ' . substr($string, $pos + 11, 40).":
             Este error se debe a que ay un valor repetido";
        }
        if (strpos($string, "1452") != false) { //n de error de violacion de 
            $msg = 'Error de integridad referencial : Este error se debe a que se esta intentando vincular algun capo de clave foranea con un id inexistente';              //integridad referencial
        }
        if (strpos($string, "1264") != false) { //n de error de violacion de desbordamiento 
            $msg = 'Error de desbordamiento o valor fuera de rango :Este error se debe a que se esta intentando insertar un valor demasiado grande en algun campo';
        }
        return $msg;
    }

}