<?php

namespace App\Http\Controllers;

use App\BranchModel;
use App\Http\Validation\Validation;
use Exception;
use Illuminate\Http\Request;

class BranchController extends Controller
{
    var $jsonResponse = ['message' => null, 'data' => null];

    public function all()
    {
        return BranchModel::all();
    }

    public function add(Request $request)
    {
        if (BranchModel::validateFields($request)) {
            $this->jsonResponse['message'] = 'Algunos campos presentan errores';
        } else {
            try {
                $this->jsonResponse['data'] = BranchModel::create($request->all());
                $this->jsonResponse['message'] = 'Sucursal agregada correctamente';
            } catch (Exception $e) {
                $this->jsonResponse['message'] = BranchModel::determinateError($e);
            }
        }
        return Response()->json($this->jsonResponse);
    }

    public function update($id, Request $request)
    {
        if (BranchModel::validateFields($request)) {
            $this->jsonResponse['message'] = 'Algunos campos presentan errores';
        } else {
            $branch = BranchModel::find($id);
            $branch->fill($request->all());
            $this->updateUser($branch);
        }

        return Response()->json($this->jsonResponse);
    }
    //verifica que el usuario halla sido modificado ademas de verificar los mismos errores q al insertar
    private function updateUser($branch)
    {
        if ($branch->isDirty()) {
            try {
                $branch->save();
                $this->jsonResponse['data'] = $branch;
                $this->jsonResponse['message'] = 'Sucursal actualizada correctamente';
            } catch (Exception $e) {
                $this->jsonResponse['message'] = Validation::determinateError($e);
            }
        } else {
            $this->jsonResponse['message'] = 'No se a modificado ningun campo';
        }
    }

    public function delete($id)
    {
        $branch = BranchModel::find($id);
        $branch->delete();
        $this->jsonResponse['data'] = $branch;
        $this->jsonResponse['message'] = 'Sucursal Eliminado';
        return Response()->json($this->jsonResponse);
    }
}
