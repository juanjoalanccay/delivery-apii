<?php

namespace App\Http\Controllers;

use App\Http\Resources\MenuResource;
use App\Http\Validation\Validation;
use App\MenuModel;
use Exception;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    var $jsonResponse = ['message' => null, 'data' => null];

    public function all()
    {
       // return MenuModel::all();
       return MenuResource::collection(MenuModel::all());
    }

    public function add(Request $request)
    {
        if (MenuModel::validateFields($request)) {
            $this->jsonResponse['message'] = 'Algunos campos presentan errores';
        } else {
            try {
                $this->jsonResponse['data'] = MenuModel::create($request->all());
                $this->jsonResponse['message'] = 'Menu agregada correctamente';
            } catch (Exception $e) {
                $this->jsonResponse['message'] = MenuModel::determinateError($e);
            }
        }
        return Response()->json($this->jsonResponse);
    }

    public function update($id, Request $request)
    {
        if (MenuModel::validateFields($request)) {
            $this->jsonResponse['message'] = 'Algunos campos presentan errores';
        } else {
            $menu = MenuModel::find($id);
            $menu->fill($request->all());
            $this->updateMenu($menu);
        }

        return Response()->json($this->jsonResponse);
    }
    //verifica que el usuario halla sido modificado ademas de verificar los mismos errores q al insertar
    private function updateMenu($menu)
    {
        if ($menu->isDirty()) {
            try {
                $menu->save();
                $this->jsonResponse['data'] = $menu;
                $this->jsonResponse['message'] = 'Menu actualizado correctamente';
            } catch (Exception $e) {
                $this->jsonResponse['message'] = Validation::determinateError($e);
            }
        } else {
            $this->jsonResponse['message'] = 'No se a modificado ningun campo';
        }
    }

    public function delete($id)
    {
        $menu = MenuModel::find($id);
        $menu->delete();
        $this->jsonResponse['data'] = $menu;
        $this->jsonResponse['message'] = 'Menu Eliminado';
        return Response()->json($this->jsonResponse);
    }
}
