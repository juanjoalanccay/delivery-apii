<?php

namespace App\Http\Controllers;

use App\Http\Resources\PersonResource;
use App\Http\Validation\Validation;
use App\PersonModel;
use Exception;
use Illuminate\Http\Request;

class PersonController extends Controller
{
    var $jsonResponse = ['message' => null, 'data' => null];

    public function all()
    {
        //return PersonModel::all();
        return PersonResource::collection(PersonModel::all());
    }
    public function logIn(Request $credentials)
    {
        //$person = PersonModel::where('email',$credentials->email)->firstOrFail();
        $person = PersonModel::all()->where('email', $credentials->email)
            ->where('password', $credentials->password);

        return PersonResource::collection($person);
    }
    public function getOne(Request $id)
    {
        $person = PersonModel::find($id);
        return PersonResource::collection($person);
    }

    public function add(Request $request)
    {
        if (PersonModel::validateFields($request)) {
            $this->jsonResponse['message'] = 'Algunos campos presentan errores';
        } else {
            try {
                $this->jsonResponse['data'] = PersonModel::create($request->all());
                $this->jsonResponse['message'] = 'Persona agregada correctamente';
            } catch (Exception $e) {
                $this->jsonResponse['message'] = Validation::determinateError($e);
            }
        }
        return Response()->json($this->jsonResponse);
    }

    public function update($id, Request $request)
    {
        if (PersonModel::validateFields($request)) {
            $this->jsonResponse['message'] = 'Algunos campos presentan errores';
        } else {
            $person = PersonModel::find($id);
            $person->fill($request->all());
            $this->updatePerson($person);
        }
        return Response()->json($this->jsonResponse);
    }


    private function updatePerson($person)
    {
        if ($person->isDirty()) {
            try {
                $person->save();
                $this->jsonResponse['data'] = $person;
                $this->jsonResponse['message'] = 'Persona actualizada correctamente';
            } catch (Exception $e) {
                $this->jsonResponse['message'] = Validation::determinateError($e);
            }
        } else {
            $this->jsonResponse['message'] = 'No se a modificado ningun campo';
        }
    }

    public function delete($id)
    {
        $person = PersonModel::find($id);
        $person->delete();
        $this->jsonResponse['data'] = $person;
        $this->jsonResponse['message'] = 'Persona Eliminada';
        return Response()->json($this->jsonResponse);
    }
}
