<?php

namespace App\Http\Controllers;

use App\Http\Validation\Validation;
use App\ItemModel;
use Exception;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    var $jsonResponse = ['message' => null, 'data' => null];

    public function all()
    {
        return ItemModel::all();
    }

    public function add(Request $request)
    {
        if (ItemModel::validateFields($request)) {
            $this->jsonResponse['message'] = 'Algunos campos presentan errores';
        } else {
            try {
                $this->jsonResponse['data'] = ItemModel::create($request->all());
                $this->jsonResponse['message'] = 'Item agregado correctamente';
            } catch (Exception $e) {
                $this->jsonResponse['message'] = Validation::determinateError($e);
            }
        }
        return Response()->json($this->jsonResponse);
    }

    public function update($id, Request $request)
    {
        if (ItemModel::validateFields($request)) {
            $this->jsonResponse['message'] = 'Algunos campos presentan errores';
        } else {
            $item = ItemModel::find($id);
            $item->fill($request->all());
            $this->updateItem($item);
        }
        return Response()->json($this->jsonResponse);
    }

    private function updateItem($item)
    {
        if ($item->isDirty()) {
            try {
                $item->save();
                $this->jsonResponse['data'] = $item;
                $this->jsonResponse['message'] = 'Item actualizado correctamente';
            } catch (Exception $e) {
                $this->jsonResponse['message'] = Validation::determinateError($e);
            }
        } else {
            $this->jsonResponse['message'] = 'No se a modificado ningun campo';
        }
    }
    public function delete($id)
    {
        $item = ItemModel::find($id);
        $item->delete();
        $this->jsonResponse['data'] = $item;
        $this->jsonResponse['message'] = 'Item Eliminado';
        return Response()->json($this->jsonResponse);
    }
}
