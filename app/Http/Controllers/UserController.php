<?php

namespace App\Http\Controllers;

use App\Http\Validation\Validation;
use App\UserModel;
use Exception;
use Illuminate\Http\Request;

class UserController extends Controller
{
    var $jsonResponse = ['message' => null, 'data' => null];

    public function all()
    {
        return UserModel::all();
    }

    public function add(Request $request)
    {
        if (UserModel::validateFields($request)) {
            $this->jsonResponse['message'] = 'Algunos campos presentan errores';
        } else {
            try {
                $this->jsonResponse['data'] = UserModel::create($request->all());
                $this->jsonResponse['message'] = 'Usuario agregado correctamente';
            } catch (Exception $e) {
                $this->jsonResponse['message'] = Validation::determinateError($e);
            }
        }
        return Response()->json($this->jsonResponse);
    }

    public function update($id, Request $request)
    {
        if (UserModel::validateFields($request)) {
            $this->jsonResponse['message'] = 'Algunos campos presentan errores';
        } else {
            $user = UserModel::find($id);
            $user->fill($request->all());
            $this->updateUser($user);
        }

        return Response()->json($this->jsonResponse);
    }
//verifica que el usuario halla sido modificado ademas de verificar los mismos errores q al insertar
    private function updateUser($user)
    {
        if ($user->isDirty()) {
            try {
                $user->save();
                $this->jsonResponse['data'] = $user;
                $this->jsonResponse['message'] = 'Usuario actualizado correctamente';
            } catch (Exception $e) {
                $this->jsonResponse['message'] = Validation::determinateError($e);
            }
        } else {
            $this->jsonResponse['message'] = 'No se a modificado ningun campo';
        }
    }

    public function delete($id)
    {
        $user = UserModel::find($id);
        $user->delete();
        $this->jsonResponse['data'] = $user;
        $this->jsonResponse['message'] = 'Usuario Eliminado';
        return Response()->json($this->jsonResponse);
    }
}
