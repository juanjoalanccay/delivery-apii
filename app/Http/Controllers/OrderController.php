<?php

namespace App\Http\Controllers;

use App\Http\Validation\Validation;
use App\OrderModel;
use Exception;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    var $jsonResponse = ['message' => null, 'data' => null];

    public function all()
    {
        return OrderModel::all();
    }

    public function add(Request $request)
    {
        if (OrderModel::validateFields($request)) {
            $this->jsonResponse['message'] = 'Algunos campos presentan errores';
        } else {
            try {
                $this->jsonResponse['data'] = OrderModel::create($request->all());
                $this->jsonResponse['message'] = 'Pedido agregada correctamente';
            } catch (Exception $e) {
                $this->jsonResponse['message'] = OrderModel::determinateError($e);
            }
        }
        return Response()->json($this->jsonResponse);
    }

    public function update($id, Request $request)
    {
        if (OrderModel::validateFields($request)) {
            $this->jsonResponse['message'] = 'Algunos campos presentan errores';
        } else {
            $order = OrderModel::find($id);
            $order->fill($request->all());
            $this->updateOrder($order);
        }

        return Response()->json($this->jsonResponse);
    }
    //verifica que el usuario halla sido modificado ademas de verificar los mismos errores q al insertar
    private function updateOrder($order)
    {
        if ($order->isDirty()) {
            try {
                $order->save();
                $this->jsonResponse['data'] = $order;
                $this->jsonResponse['message'] = 'Pedido actualizado correctamente';
            } catch (Exception $e) {
                $this->jsonResponse['message'] = Validation::determinateError($e);
            }
        } else {
            $this->jsonResponse['message'] = 'No se a modificado ningun campo';
        }
    }

    public function delete($id)
    {
        $order = OrderModel::find($id);
        $order->delete();
        $this->jsonResponse['data'] = $order;
        $this->jsonResponse['message'] = 'Pedido Eliminado';
        return Response()->json($this->jsonResponse);
    }
}
