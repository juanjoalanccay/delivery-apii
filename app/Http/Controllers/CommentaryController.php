<?php

namespace App\Http\Controllers;

use App\CommentaryModel;
use App\Http\Resources\CommentaryResource;
use App\Http\Validation\Validation;
use Exception;
use Illuminate\Http\Request;

class CommentaryController extends Controller
{
    var $jsonResponse = ['message' => null, 'data' => null];

    public function all()
    {
        //return CommentaryModel::all();
        return CommentaryResource::collection(CommentaryModel::all());
    }

    public function add(Request $request)
    {
        if (CommentaryModel::validateFields($request)) {
            $this->jsonResponse['message'] = 'Algunos campos presentan errores';
        } else {
            try {
                $this->jsonResponse['data'] = CommentaryModel::create($request->all());
                $this->jsonResponse['message'] = 'Comentario agregado correctamente';
            } catch (Exception $e) {
                $this->jsonResponse['message'] = CommentaryModel::determinateError($e);
            }
        }
        return Response()->json($this->jsonResponse);
    }

    public function update($id, Request $request)
    {
        if (CommentaryModel::validateFields($request)) {
            $this->jsonResponse['message'] = 'Algunos campos presentan errores';
        } else {
            $commentary = CommentaryModel::find($id);
            $commentary->fill($request->all());
            $this->updateCommentary($commentary);
        }

        return Response()->json($this->jsonResponse);
    }
    //verifica que el usuario halla sido modificado ademas de verificar los mismos errores q al insertar
    private function updateCommentary($commentary)
    {
        if ($commentary->isDirty()) {
            try {
                $commentary->save();
                $this->jsonResponse['data'] = $commentary;
                $this->jsonResponse['message'] = 'Sucursal actualizada correctamente';
            } catch (Exception $e) {
                $this->jsonResponse['message'] = Validation::determinateError($e);
            }
        } else {
            $this->jsonResponse['message'] = 'No se a modificado ningun campo';
        }
    }

    public function delete($id)
    {
        $commentary = CommentaryModel::find($id);
        $commentary->delete();
        $this->jsonResponse['data'] = $commentary;
        $this->jsonResponse['message'] = 'Comentario Eliminado';
        return Response()->json($this->jsonResponse);
    }
}
