<?php

namespace App\Http\Controllers;

use App\DeliveryModel;
use App\Http\Validation\Validation;
use Exception;
use Illuminate\Http\Request;

class DeliveryController extends Controller
{
    var $jsonResponse = ['message' => null, 'data' => null];

    public function all()
    {
        return DeliveryModel::all();
    }

    public function add(Request $request)
    {
        if (DeliveryModel::validateFields($request)) {
            $this->jsonResponse['message'] = 'Algunos campos presentan errores';
        } else {
            try {
                $this->jsonResponse['data'] = DeliveryModel::create($request->all());
                $this->jsonResponse['message'] = 'Delivery agregado correctamente';
            } catch (Exception $e) {
                $this->jsonResponse['message'] = Validation::determinateError($e);
            }
        }
        return Response()->json($this->jsonResponse);
    }

    public function update($id, Request $request)
    {
        if (DeliveryModel::validateFields($request)) {
            $this->jsonResponse['message'] = 'Algunos campos presentan errores';
        } else {
            $delivery = DeliveryModel::find($id);
            $delivery->fill($request->all());
            $this->updateDelivery($delivery);
        }
        return Response()->json($this->jsonResponse);
    }

    private function updateDelivery($delivery)
    {
        if ($delivery->isDirty()) {
            try {
                $delivery->save();
                $this->jsonResponse['data'] = $delivery;
                $this->jsonResponse['message'] = 'Delivery actualizado correctamente';
            } catch (Exception $e) {
                $this->jsonResponse['message'] = Validation::determinateError($e);
            }
        } else {
            $this->jsonResponse['message'] = 'No se a modificado ningun campo';
        }
    }
    public function delete($id)
    {
        $delivery = DeliveryModel::find($id);
        $delivery->delete();
        $this->jsonResponse['data'] = $delivery;
        $this->jsonResponse['message'] = 'Delivery Eliminado';
        return Response()->json($this->jsonResponse);
    }
}
