<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PersonResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        
        return [
            'id' => $this->id,
            'name' => $this->name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'phone' => $this->phone,
            'password' => $this->password,
            'photo' => $this->photo,
            'dni' => $this->dni,
            'isEmployee'=>$this->delivery!=null, // podriamos optar por solo traer este boolean pero
            'user' => $this->user,              // despues deberiamos consultar de nvo para traer los demas datos
            'delivery'=>$this->delivery,
        ];
    }
}
