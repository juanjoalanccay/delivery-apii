<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderModel extends Model
{
    public static function validateFields($object)
    {
        return is_null($object->state) || is_null($object->location) || is_null($object->street)||
        is_null($object->street_number)||is_null($object->total)||is_null($object->id_user)||
        is_null($object->id_branch);
    }
    protected $table ='order';
    protected $fillable=array('state','location','street','street_number','date','total','id_user','id_branch');
    public $timestamps = false;//cancela los campos automaticos created_at y updated_at

    public function user()
    {
        return $this->belongsTo(UserModel::class);
    }
    public function items()
    {
        return $this->hasMany(ItemModel::class,'nro_order');
    }

    public function branchOffice()
    {
        return $this->belongsTo(BranchModel::class);
    }
}
