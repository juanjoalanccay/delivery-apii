<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserModel extends Model
{
    public static function validateFields($object)
    {
        return is_null($object->rol) || is_null($object->id_person);
    }

    protected $table = 'user';
    protected $fillable = array('rol', 'id_person');
    public $timestamps = false; //cancela los campos automaticos created_at y updated_at

    public function person()
    {
        return $this->belongsTo(PersonModel::class);
    }
    public function orders()
    {
        return $this->hasMany(OrderModel::class, 'id_user');
    }
    public function commentaries()
    {
        return $this->hasMany(CommentaryModel::class, 'id_user');
    }
}
