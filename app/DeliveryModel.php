<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryModel extends Model
{
    public static function validateFields($object)
    {
        return is_null($object->cuil) || is_null($object->patent)
            || is_null($object->id_branch) || is_null($object->id_person);
    }

    protected $table ='delivery';
    protected $fillable=array('cuil','patent','id_branch','id_person');
    public $timestamps = false;//cancela los campos automaticos created_at y updated_at
    
    public function person()
    {
        return $this->belongsTo(PersonModel::class);
    }
    public function branchOffice()
    {
        return $this->belongsTo(BranchModel::class);
    }
}
