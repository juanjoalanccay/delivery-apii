<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BranchModel extends Model
{
    public static function validateFields($object)
    {
        return is_null($object->name) || is_null($object->location) || is_null($object->street) 
        || is_null($object->phone) || is_null($object->street_number) || is_null($object->nro_sucursal);
    }

    protected $table ='branch_office';
    protected $fillable=array('name','location','street','phone','street_number','nro_sucursal');
    public $timestamps = false;//cancela los campos automaticos created_at y updated_at
   
    public function deliverys()
    {
        return $this->hasMany(DeliveryModel::class,'id_branch');
    }
}
