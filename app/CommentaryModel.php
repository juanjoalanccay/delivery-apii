<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentaryModel extends Model
{
    public static function validateFields($object)
    {
        return is_null($object->content) || is_null($object->stars)
            || is_null($object->id_menu) || is_null($object->id_user);
    }
    protected $table ='commentary';
    protected $fillable=array('content','stars','id_menu','id_user');
    public $timestamps = false;//cancela los campos automaticos created_at y updated_at

    public function menu()
    {
        return $this->belongsTo(MenuModel::class,'id');
    }
    public function user()
    {
        return $this->belongsTo(UserModel::class,'id_user','id');
    }
}
