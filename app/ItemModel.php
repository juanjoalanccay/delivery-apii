<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemModel extends Model
{
    public static function validateFields($object)
    {
        return is_null($object->quantity) || is_null($object->total)
            || is_null($object->id_menu) || is_null($object->nro_order);
    }
    protected $table ='item_order';
    protected $fillable=array('quantity','total','id_menu','nro_order');
    public $timestamps = false;//cancela los campos automaticos created_at y updated_at
    public function order()
    {
        return $this->hasMany(OrderModel::class);
    }
}
