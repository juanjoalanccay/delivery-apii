<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//routes person
Route::get('person','PersonController@all');
Route::post('person/log','PersonController@getOne');
Route::post('login','PersonController@logIn');
Route::post('person','PersonController@add');
Route::post('person/{id}','PersonController@update');
Route::delete('person/{id}','PersonController@delete');

//routes delivery
Route::get('delivery','DeliveryController@all');
Route::post('delivery','DeliveryController@add');
Route::post('delivery/{id}','DeliveryController@update');
Route::delete('delivery/{id}','DeliveryController@delete');

//routes user
Route::get('user','UserController@all');
Route::post('user','UserController@add');
Route::post('user/{id}','UserController@update');
Route::delete('user/{id}','UserController@delete');

//routes branch_office
Route::get('branch','BranchController@all');
Route::post('branch','BranchController@add');
Route::post('branch/{id}','BranchController@update');
Route::delete('branch/{id}','BranchController@delete');

//routes commentary
Route::get('commentary','CommentaryController@all');
Route::post('commentary','CommentaryController@add');
Route::post('commentary/{id}','CommentaryController@update');
Route::delete('commentary/{id}','CommentaryController@delete');
//routes menu
Route::get('menu','MenuController@all');
Route::post('menu','MenuController@add');
Route::post('menu/{id}','MenuController@update');
Route::delete('menu/{id}','MenuController@delete');
//routes order
Route::get('order','OrderController@all');
Route::post('order','OrderController@add');
Route::post('order/{id}','OrderController@update');
Route::delete('order/{id}','OrderController@delete');
//routes item
Route::get('item','ItemController@all');
Route::post('item','ItemController@add');
Route::post('item/{id}','ItemController@update');
Route::delete('item/{id}','ItemController@delete');