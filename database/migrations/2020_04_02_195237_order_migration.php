<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OrderMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->engine = 'InnoDB'; 
            $table->bigIncrements('nro_order');
            $table->string('state',15);
            $table->string('location',30);
            $table->string('street',30);
            $table->integer('street_number');
            $table->dateTime('date');
            $table->double('total');
        
            $table->unsignedBigInteger('id_user');
            $table->unsignedBigInteger('id_delivery');

            $table->foreign('id_user')->references('id')->on('user')->onDelete('cascade');
            $table->foreign('id_delivery')->references('id')->on('delivery');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
