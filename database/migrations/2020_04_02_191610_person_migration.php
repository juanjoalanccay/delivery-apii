<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PersonMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('person', function (Blueprint $table) {
            $table->engine = 'InnoDB'; //quisas al ser innoDb el motor , no nos deje insertar id_branch nulas
            $table->bigIncrements('id');
            $table->string('name',20);
            $table->string('last_name',20);
            $table->string('email',35)->unique();
            $table->string('phone',18);
            $table->string('password',20);
            $table->mediumText('photo')->nullable();
            $table->integer('dni');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('person');
    }
}
