<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DeliveryMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery', function (Blueprint $table) {
            $table->engine = 'InnoDB'; //quisas al ser innoDb el motor , no nos deje insertar id_branch nulas
            $table->bigIncrements('id');
            $table->string('cuil',20);
            $table->string('patent',20);
            $table->unsignedBigInteger('id_branch');
            $table->unsignedBigInteger('id_person');
            
            $table->foreign('id_branch')->references('id')->on('branch_office')->onDelete('cascade');
            $table->foreign('id_person')->references('id')->on('person')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery');
    }
}
