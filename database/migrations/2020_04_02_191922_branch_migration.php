<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BranchMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_office', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('name',30);
            $table->string('location',30);
            $table->string('street',30);
            $table->string('phone',18);
            $table->integer('street_number');
            $table->integer('nro_sucursal')->unique();
            //ej: $table->unique(['name','phone','dni']); para crer indeces perzonalizados
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_office');
    }
}
