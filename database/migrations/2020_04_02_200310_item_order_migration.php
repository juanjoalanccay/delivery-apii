<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ItemOrderMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_order', function (Blueprint $table) {
            $table->engine = 'InnoDB'; 
            $table->bigIncrements('id');
            $table->integer('quantity');
            $table->double('total');
        
            $table->unsignedBigInteger('id_menu');
            $table->unsignedBigInteger('nro_order');
            // al eliminar un menu (platillo) no se deben eliminar los items relacionados
            $table->foreign('id_menu')->references('id')->on('menu');
            //al eliminar una orden o poner en estado de entregado deberia borrarse todos los items
            //solo si queremos liberar memoria
            $table->foreign('nro_order')->references('nro_order')->on('order')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_order');
    }
}
